package com.freewayso.image.combiner.enums;

/**
 * @Author zhaoqing.chen
 * @Date 2020/8/21
 * @Description 自动换行时，多行文本的对齐方式
 **/

public enum LineAlign {
    /**
     * 左对齐
     */
    Left,
    /**
     * 居中对齐
     */
    Center,
    /**
     * 右对齐
     */
    Right
}
